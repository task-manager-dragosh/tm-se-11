package ru.dragosh.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.exception.AccessForbiddenException;
import ru.dragosh.tm.exception.EntityIsAlreadyExistException;

import java.util.List;

public interface SessionRepository {
    List<Session> findAll();
    Session findOne(@NotNull String userId) throws Exception;
    void persist(@NotNull Session session) throws EntityIsAlreadyExistException, AccessForbiddenException;
    void merge(@NotNull String sessionId, @NotNull Session session);
    void remove(@NotNull String sessionId) throws Exception;
    void removeAll() throws Exception;
    boolean contains(@NotNull final String id);
}
