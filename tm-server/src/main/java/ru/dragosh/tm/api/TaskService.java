package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Task;

import java.text.ParseException;
import java.util.List;

public interface TaskService {
    List<Task> findAll(String userId, String projectId);
    Task find(String userId, String projectId, String nameTask);
    void persist(Task task) throws ParseException;
    void merge(Task task);
    void remove(String userId, String taskId);
    void removeAll(String userId, String projectId);

    List<Task> getEntitiesList();
    void loadEntities(List<Task> entities);

    List<Task> findByStringPart(String userId, String projectId, String str);

    List<Task> getSortedBySystemTime(String userId);
    List<Task> getSortedByDateStart(String userId);
    List<Task> getSortedByDateFinish(String userId);
    List<Task> getSortedByStatus(String userId);
}