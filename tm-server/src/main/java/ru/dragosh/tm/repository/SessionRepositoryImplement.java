package ru.dragosh.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.api.SessionRepository;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.exception.EntityIsAlreadyExistException;
import ru.dragosh.tm.exception.EntityListIsEmptyException;
import ru.dragosh.tm.exception.EntityNotExistsException;

import java.util.*;

public final class SessionRepositoryImplement implements SessionRepository {
    @NotNull
    private final Map<String, Session> sessionMap = new HashMap<>();

    @NotNull
    @Override
    public List<Session> findAll() {
        final boolean mapIsEmpty = sessionMap.isEmpty();
        if (mapIsEmpty) return Collections.emptyList();
        return new ArrayList<>(sessionMap.values());
    }

    @NotNull
    @Override
    public Session findOne(@NotNull String sessionId) throws Exception {
        final boolean mapIsEmpty = sessionMap.isEmpty();
        if (mapIsEmpty) throw new EntityListIsEmptyException();
        final boolean mapContainsKey = sessionMap.containsKey(sessionId);
        if (mapContainsKey) return sessionMap.get(sessionId);
        throw new EntityNotExistsException();
    }

    @Override
    public void persist(@NotNull Session session) throws EntityIsAlreadyExistException {
        @NotNull final String entityId = session.getId();
        final boolean mapContainsKey = sessionMap.containsKey(entityId);
        if(mapContainsKey) throw new EntityIsAlreadyExistException();
        else sessionMap.put(session.getId(), session);
    }

    @Override
    public void merge(@NotNull String sessionId, @NotNull Session session) {
        session.setId(sessionId);
        sessionMap.put(sessionId, session);
    }

    @Override
    public void remove(@NotNull String sessionId) throws Exception {
        final boolean mapIsEmpty = sessionMap.isEmpty();
        if(mapIsEmpty)throw new EntityListIsEmptyException();
        final boolean mapContainsKey = sessionMap.containsKey(sessionId);
        if(!mapContainsKey) throw new EntityNotExistsException();
        else sessionMap.remove(sessionId);
    }

    @Override
    public void removeAll() throws Exception {
        final boolean mapIsEmpty = sessionMap.isEmpty();
        if(mapIsEmpty) return;
        else sessionMap.clear();
    }

    @Override
    public boolean contains(@NotNull String id) {
        return sessionMap.containsKey(id);
    }
}
