package ru.dragosh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.UserRepository;
import ru.dragosh.tm.entity.User;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class UserRepositoryImplement implements UserRepository {
    @NotNull
    private final Map<String, User> users = new LinkedHashMap<>();

    @Nullable
    @Override
    public User find(@NotNull final String login, @NotNull final String password) {
        return users.values().stream()
                .filter(user -> user.getLogin().equals(login) && user.getPassword().equals(password))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return users.values().stream()
                .filter(user -> user.getLogin().equals(login))
                .findFirst().orElse(null);
    }

    @Override
    public User findById(String id) {
        return users.get(id);
    }

    @Override
    public void persist(@NotNull final User user) {
        users.put(user.getId(), user);
    }

    @Override
    public void merge(@NotNull final User user) {
        users.put(user.getId(), user);
    }

    @Override
    public void remove(@NotNull final String userId) {
        users.remove(userId);
    }

    @Override
    public List<User> getEntitiesList() {
        return new ArrayList<>(users.values());
    }

    @Override
    public void loadEntities(List<User> entities) {
        users.clear();
        entities.forEach(user -> users.put(user.getId(), user));
    }
}
