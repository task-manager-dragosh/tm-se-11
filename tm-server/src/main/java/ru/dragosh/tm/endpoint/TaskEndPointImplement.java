package ru.dragosh.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.ServiceLocator;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.entity.containters.TaskList;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.exception.AccessForbiddenException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@NoArgsConstructor
@WebService(endpointInterface = "ru.dragosh.tm.endpoint.TaskEndPoint")
public final class TaskEndPointImplement implements TaskEndPoint {
    @NotNull
    private ServiceLocator serviceLocator;

    public TaskEndPointImplement(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public TaskList findAll(@WebParam(name = "session") @Nullable final Session session,
                            @WebParam(name = "projectId") @Nullable final String projectId) throws Exception{
        if (session == null)
            throw new AccessForbiddenException();
        if (projectId == null)
            throw new AccessForbiddenException();
        if (projectId.isEmpty())
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().validateRole(session, RoleType.USER, RoleType.ADMIN);
        TaskList taskList = new TaskList();
        taskList.setTaskList(serviceLocator.getTaskService().findAll(session.getUserId(), projectId));
        return taskList;
    }

    @WebMethod
    public Task find(@WebParam(name = "session") @Nullable final Session session,
                     @WebParam(name = "projectId") @Nullable final String projectId,
                     @WebParam(name = "taskName") @Nullable final String taskName) throws Exception {
        if (session == null)
            throw new AccessForbiddenException();
        if (projectId == null || projectId.isEmpty())
            throw new AccessForbiddenException();
        if (taskName == null || taskName.isEmpty())
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().validateRole(session, RoleType.USER, RoleType.ADMIN);
        return serviceLocator.getTaskService().find(session.getUserId(), projectId, taskName);
    }

    @WebMethod
    public void removeAll(@WebParam(name = "session") @Nullable final Session session,
                          @WebParam(name = "projectId") @Nullable final String projectId) throws Exception {
        if (session == null)
            throw new AccessForbiddenException();
        if (projectId == null || projectId.isEmpty())
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().validateRole(session, RoleType.USER, RoleType.ADMIN);
        serviceLocator.getTaskService().removeAll(session.getUserId(), projectId);
    }

    @WebMethod
    public TaskList findByStringPart(@WebParam(name = "session") @Nullable final Session session,
                                     @WebParam(name = "projectId") @Nullable final String projectId,
                                     @WebParam(name = "part") @Nullable final String part
    ) throws Exception {
        if (session == null)
            throw new AccessForbiddenException();
        if (projectId == null || projectId.isEmpty())
            throw new AccessForbiddenException();
        if (part == null || part.isEmpty())
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().validateRole(session, RoleType.USER, RoleType.ADMIN);
        TaskList taskList = new TaskList();
        taskList.setTaskList(serviceLocator.getTaskService().findByStringPart(session.getUserId(), projectId, part));
        return taskList;
    }

    @WebMethod
    public void persist(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "task") @Nullable final Task task) throws Exception {
        if (session == null)
            throw new AccessForbiddenException();
        if (task == null)
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().validateRole(session, RoleType.USER, RoleType.ADMIN);
        serviceLocator.getSessionService().validateRole(session, RoleType.USER, RoleType.ADMIN);
        SimpleDateFormat dt = new SimpleDateFormat("dd.mm.yyyy");
        task.setDateStart(dt.format(dt.parse(task.getDateStart())));
        task.setDateFinish(dt.format(dt.parse(task.getDateFinish())));
        serviceLocator.getTaskService().persist(task);

    }

    @WebMethod
    public void merge(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "task") @Nullable final Task task) throws Exception {
        if (session == null)
            throw new AccessForbiddenException();
        if (task == null)
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().validateRole(session, RoleType.USER, RoleType.ADMIN);
        serviceLocator.getTaskService().merge(task);
    }

    @WebMethod
    public void remove(@WebParam(name = "session") @Nullable final Session session,
                       @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().validateRole(session, RoleType.USER, RoleType.ADMIN);
        serviceLocator.getTaskService().remove(session.getUserId(), id);
    }

    @WebMethod
    public TaskList getSortedBySystemTime(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null)
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().validateRole(session, RoleType.USER, RoleType.ADMIN);
        TaskList taskList = new TaskList();
        taskList.setTaskList(serviceLocator.getTaskService().getSortedBySystemTime(session.getUserId()));
        return taskList;
    }

    @WebMethod
    public TaskList getSortedByDateStart(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null)
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().validateRole(session, RoleType.USER, RoleType.ADMIN);
        TaskList taskList = new TaskList();
        taskList.setTaskList(serviceLocator.getTaskService().getSortedByDateStart(session.getUserId()));
        return taskList;
    }

    @WebMethod
    public TaskList getSortedByDateFinish(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null)
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().validateRole(session, RoleType.USER, RoleType.ADMIN);
        TaskList taskList = new TaskList();
        taskList.setTaskList(serviceLocator.getTaskService().getSortedByDateFinish(session.getUserId()));
        return taskList;
    }

    @WebMethod
    public TaskList getSortedByStatus(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null)
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().validateRole(session, RoleType.USER, RoleType.ADMIN);
        TaskList taskList = new TaskList();
        taskList.setTaskList(serviceLocator.getTaskService().getSortedByStatus(session.getUserId()));
        return taskList;
    }
}
