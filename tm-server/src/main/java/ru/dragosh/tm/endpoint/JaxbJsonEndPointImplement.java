package ru.dragosh.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.ServiceLocator;
import ru.dragosh.tm.entity.Domain;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.exception.AccessForbiddenException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.io.IOException;

@NoArgsConstructor
@WebService
public final class JaxbJsonEndPointImplement implements JaxbJsonEndPoint {
    @NotNull
    private ServiceLocator serviceLocator;

    public JaxbJsonEndPointImplement(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public void save(@Nullable final Session session) throws Exception {
        if (session == null)
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().validate(session, RoleType.ADMIN);
        @NotNull Domain domain = new Domain();
        domain.setUserList(serviceLocator.getUserService().getEntitiesList());
        domain.setProjectList(serviceLocator.getProjectService().getEntitiesList());
        domain.setTaskList(serviceLocator.getTaskService().getEntitiesList());
        serviceLocator.getJaxbJsonServiceImplement().save(domain);
    }

    @WebMethod
    public void load(@Nullable final Session session) throws Exception {
        if (session == null)
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().validate(session, RoleType.ADMIN);
        @Nullable final Domain domain = serviceLocator.getJaxbJsonServiceImplement().Load();
        if (domain == null)
            throw new AccessForbiddenException();
        serviceLocator.getTaskService().loadEntities(domain.getTaskList());
        serviceLocator.getUserService().loadEntities(domain.getUserList());
        serviceLocator.getProjectService().loadEntities(domain.getProjectList());
        serviceLocator.getSessionService().closeSession(session);
    }
}
