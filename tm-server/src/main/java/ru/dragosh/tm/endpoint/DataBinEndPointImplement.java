package ru.dragosh.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.ServiceLocator;
import ru.dragosh.tm.entity.Domain;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.exception.AccessForbiddenException;

import javax.jws.WebService;

@NoArgsConstructor
@WebService
public final class DataBinEndPointImplement implements DataBinEndPoint {
    @NotNull
    private ServiceLocator serviceLocator;

    public DataBinEndPointImplement(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public void save(@Nullable final Session session) throws Exception {
        if (session == null)
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().validate(session, RoleType.ADMIN);
        @NotNull Domain domain = new Domain();
        domain.setUserList(serviceLocator.getUserService().getEntitiesList());
        domain.setProjectList(serviceLocator.getProjectService().getEntitiesList());
        domain.setTaskList(serviceLocator.getTaskService().getEntitiesList());
        serviceLocator.getDataBinServiceImplement().save(domain);
    }

    public void load(@Nullable final Session session) throws Exception {
        if (session == null)
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().validate(session, RoleType.ADMIN);
        @Nullable Domain domain = serviceLocator.getDataBinServiceImplement().Load();
        if (domain == null)
            throw new AccessForbiddenException();
        serviceLocator.getTaskService().loadEntities(domain.getTaskList());
        serviceLocator.getUserService().loadEntities(domain.getUserList());
        serviceLocator.getProjectService().loadEntities(domain.getProjectList());
        serviceLocator.getSessionService().closeSession(session);
    }
}
