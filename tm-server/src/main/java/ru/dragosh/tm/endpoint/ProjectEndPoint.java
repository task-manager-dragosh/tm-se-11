package ru.dragosh.tm.endpoint;

import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.entity.containters.ProjectList;
import ru.dragosh.tm.exception.AccessForbiddenException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.text.ParseException;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface ProjectEndPoint {
    @WebMethod
    ProjectList findAll(Session session) throws AccessForbiddenException, Exception;

    @WebMethod
    Project find(String projectName, Session session) throws Exception;

    @WebMethod
    void persist(Session session, Project project) throws Exception;

    @WebMethod
    void merge(Session session, Project project) throws Exception;

    @WebMethod
    void remove(Session session, String projectId) throws Exception;

    @WebMethod
    void removeAll(Session session) throws Exception;

    @WebMethod
    ProjectList findByStringPart(Session session, String str) throws Exception;

    @WebMethod
    ProjectList getSortedBySystemTime(Session session) throws Exception;

    @WebMethod
    ProjectList getSortedByDateStart(Session session) throws Exception;

    @WebMethod
    ProjectList getSortedByDateFinish(Session session) throws Exception;

    @WebMethod
    ProjectList getSortedByStatus(Session session) throws Exception;
}
