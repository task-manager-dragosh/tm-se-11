package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.UserRepository;
import ru.dragosh.tm.api.UserService;
import ru.dragosh.tm.entity.User;

import java.util.List;

public final class UserServiceImplement implements UserService {
    @NotNull
    private final UserRepository userRepository;

    public UserServiceImplement(@NotNull final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Nullable
    @Override
    public User find(@NotNull final String login, @NotNull final String password) {
        if (login == null || login.isEmpty())
            return null;
        if (password == null || password.isEmpty())
            return null;
        return userRepository.find(login, password);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        if (login == null || login.isEmpty())
            return null;
        return userRepository.findByLogin(login);
    }

    @Override
    public User findById(String id) {
        return userRepository.findById(id);
    }

    @Override
    public void persist(@NotNull final User user) {
        if (user == null)
            return;
        userRepository.persist(user);
    }

    @Override
    public void merge(@NotNull final User user) {
        if (user == null)
            return;
        userRepository.merge(user);
    }

    @Override
    public void remove(@NotNull final String userId) {
        if (userId == null || userId.isEmpty())
            return;
        userRepository.remove(userId);
    }

    @NotNull
    @Override
    public List<User> getEntitiesList() {
        return userRepository.getEntitiesList();
    }

    @Override
    public void loadEntities(@NotNull final List<User> entities) {
        if (entities == null)
            return;
        userRepository.loadEntities(entities);
    }
}
