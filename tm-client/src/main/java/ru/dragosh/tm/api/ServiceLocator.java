package ru.dragosh.tm.api;

import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.endpoint.service.*;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.entity.User;

import java.util.Map;

public interface ServiceLocator {
    DataBinEndPointService getDataBinEndPointService();
    FasterJsonEndPointService getFasterJsonEndPointService();
    FasterXmlEndPointService getFasterXmlEndPointService();
    JaxbJsonEndPointService getJaxbJsonEndPointService();
    JaxbXmlEndPointService getJaxbXmlEndPointService();
    ProjectEndPointService getProjectEndPointService();
    TaskEndPointService getTaskEndPointService();
    UserEndPointService getUserEndPointService();

    Map<String, AbstractCommand> getCommands();
    Session getCurrentSession();
    void setCurrentSession(Session session);
}
